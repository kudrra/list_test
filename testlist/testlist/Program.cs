﻿using System.Collections;
using System.Diagnostics;

namespace testlist
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var Nums = new List<int> { };
            LinkedList<int> LinkedListNums = new LinkedList<int>(Nums);
            ArrayList ALNums = new ArrayList();
            Random rand = new Random();
            var timer = new Stopwatch();
            
            Console.WriteLine("\n\nЗаповнення списку даними");
            timer.Start();
            for (int i = 0; i < 100000; i++)
            {
                LinkedListNums.AddLast(rand.Next(0, 10000));
            }
            timer.Stop();
            Console.WriteLine("LinkedList: " + timer.Elapsed);

            timer.Start();
            for (int i = 0; i < 100000; i++)
            {
                ALNums.Add(rand.Next(0, 10000));
            }

            timer.Stop();
            Console.WriteLine("ArrayList: " + timer.Elapsed);

            
            Console.WriteLine("\n\nОтримання значення за iндексом 1337");

            int res = 0;
            timer.Start();
            foreach (int num in LinkedListNums)
            {
                if (res == 1337)
                {
                    res = num; break;
                }
                res++;
            }
            timer.Stop();
            Console.WriteLine("Num == " + res + "  LinkedList: " + timer.Elapsed);

            timer.Start();
            res = Convert.ToInt32(ALNums[5809]);
            timer.Stop();
            Console.WriteLine("Num == " + res + "  ArrayList: " + timer.Elapsed);
            

            Console.WriteLine("\n\nВставка першим");
            timer.Start();
            LinkedListNums.AddFirst(1488);
            timer.Stop();
            Console.WriteLine("LinkedList: " + timer.Elapsed + " Count: " + LinkedListNums.Count);
            timer.Start();
            ALNums.Insert(0, 1488);
            timer.Stop();
            Console.WriteLine("ArrayList: " + timer.Elapsed + " Count: " + ALNums.Count);
            

            Console.WriteLine("\n\nВставка за iндексом 4999");

            timer.Start();
            var CurrNode = LinkedListNums.First;
            for (int i = 0; i < LinkedListNums.Count; i++)
            {
                if (i == 4999)
                {
                    LinkedListNums.AddAfter(CurrNode, 6666);
                    break;
                }
                CurrNode = CurrNode.Next;
            }
            timer.Stop();
            Console.WriteLine("LinkedList: " + timer.Elapsed + " Count: " + LinkedListNums.Count);
            timer.Start();
            ALNums.Insert(4999, 6666);
            timer.Stop();
            Console.WriteLine("ArrayList: " + timer.Elapsed + " Count: " + ALNums.Count);
            Console.ReadKey();
        }
    }
}
